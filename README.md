Our EEE3088F PiHAT project is an Uninterruptible Power Supply microHAT that will be attached to a Raspberry Pi
Zero board. The main uses of our UPS will be to act as back-up power source when the main power
source is unavailable, it can also act as a protective measure to prevent the Raspberry Pi Zero from
suddenly shutting down during power surges and lastly it can also be used as power bank for Raspberry
Pi board in portable devices like Cameras or mobile routers.




The system specifications include:
S1: The UPS provides constant power input to the Raspberry Pi Zero board
S2: It is able to Power the Raspberry Pi board for a specific time
S3: UPS board adds to physical specifications of a Raspberry Pi microHAT
S4: UPS board is small enough to fit within the required portable device

